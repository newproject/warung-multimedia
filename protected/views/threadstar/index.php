<?php
$this->breadcrumbs=array(
	'Threadstars',
);

$this->menu=array(
	array('label'=>'Buat Threadstar', 'url'=>array('create')),
	array('label'=>'Pengaturan Thread', 'url'=>array('admin')),
);
?>

<h1>Threadstars</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
