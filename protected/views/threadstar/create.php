<?php
$this->breadcrumbs=array(
	'Threadstars'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Daftar Threadstar', 'url'=>array('index')),
	array('label'=>'Pengaturan Thread', 'url'=>array('admin')),
);
?>

<h1>Create Threadstar</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>