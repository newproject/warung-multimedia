<?php
$this->breadcrumbs=array(
	'Kategoris',
);

$this->menu=array(
	array('label'=>'Create Kategori', 'url'=>array('create'), 'visible'=>Yii::app()->user->getLevel()==1),
	array('label'=>'Manage Kategori', 'url'=>array('admin'), 'visible'=>Yii::app()->user->getLevel()==1),
);
?>

<h1>Pilih Kategori Forum Yang Anda Sukai</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
	'summaryText'=>'',
)); ?>
