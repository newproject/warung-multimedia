 <!DOCTYPE HTML>
    <html lang="en">
    <head>
    <meta charset="utf-8">
    <title>Warung Multimedia</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Styles -->
    <link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/style.css" rel="stylesheet">
 
    <!--[if lt IE 7]>
            <link href="css/fontello-ie7.css" type="text/css" rel="stylesheet">  
        <![endif]-->
    <!-- Google Web fonts -->
    <link href='http://fonts.googleapis.com/css?family=Quattrocento:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Patua+One' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <style>
    body {
        padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
    }
    </style>
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    <!-- Favicon -->
    <link rel="shortcut icon" href="img/favicon.ico">
    <!-- JQuery -->
    
    </head>
    <body>
    <!--******************** NAVBAR ********************-->
    <div class="navbar-wrapper">
      <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="navbar-inner">
          <div class="container">
            <!-- Responsive Navbar Part 1: Button for triggering responsive navbar (not covered in tutorial). Include responsive CSS to utilize. -->
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </a>
            <h1 class="brand"><a href="<?php echo Yii::app()->baseUrl; ?>">Warung Multimedia</a></h1>
            <!-- Responsive Navbar Part 2: Place all navbar contents you want collapsed withing .navbar-collapse.collapse. -->
            <nav class="pull-right nav-collapse collapse">
              
			 	
	<div class="container" id="page">
	<div id="mainmenu">
		<?php
		$kategoriAll=array(); 
		$kategori=Kategori::model()->findAll();
		foreach ($kategori as $i=>$ii)
		{
			$kategoriAll[]=array('label'=>$ii["kategori"],'url'=>array('/kategori/view','id'=>$ii["id"]));
		}
		$this->widget('ext.mbmenu.MbMenu',array(
			'items'=>array(
				array('label'=>'Home', 'url'=>array('/site/index')),
				array('label'=>'Forum', 'url'=>array('/kategori/index'),
					'items'=>$kategoriAll,
				),
				array('label'=>'Kelola Kategori', 'url'=>array('/kategori/admin'), 'visible'=>Yii::app()->user->getLevel()<=2),
				array('label'=>'Kelola Thread', 'url'=>array('/thread/admin'), 'visible'=>Yii::app()->user->getLevel()<=2),
				array('label'=>'Kelola Komentar', 'url'=>array('/comment/admin'), 'visible'=>Yii::app()->user->getLevel()<=2),
				array('label'=>'Kelola Berita', 'url'=>array('/news/admin'), 'visible'=>Yii::app()->user->getLevel()<=2),
				array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
				array('label'=>'Register', 'url'=>array('/user/create'), 'visible'=>Yii::app()->user->isGuest),
				array('label'=>'Profile ('.Yii::app()->user->name.')', 'url'=>array('/user/view','id'=>Yii::app()->user->id), 'visible'=>!Yii::app()->user->isGuest),
				array('label'=>'Logout', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
			),
		)); ?>
		
	</div><!-- mainmenu -->
        <!-- /.navbar-inner -->
      </div>
      <!-- /.navbar -->
    </div>
    <!-- /.navbar-wrapper --><!-- ******************** HeaderWrap ********************--><!--******************** Feature ********************--><!--******************** Portfolio Section ********************--><!--******************** Services Section ********************--><!--******************** Testimonials Section ********************--><!--******************** News Section ********************--><!--******************** Team Section ********************-->
    <section id="team" class="single-page scrollblock">
      <div class="container">
    <div class="featurette" style="text-align:left"> <?php echo $content; ?>
        </div>

      </div>

    </section>
    <!--******************** Contact Section ********************-->
    <section id="contact" class="single-page scrollblock">
      <!-- /.container -->
</section>
    
    <div class="footer-wrapper">
      <div class="container">
        <footer>
          <small>KELOMPOK 3IF4 - LPKIA BANDUNG.</small>        </footer>
      </div>
      <!-- ./container -->
    </div>
    <!-- Loading the javaScript at the end of the page -->
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="js/site.js"></script>
    
    
    </body>
    </html>

