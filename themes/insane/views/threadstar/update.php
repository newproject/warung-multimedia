<?php
$this->breadcrumbs=array(
	'Threadstars'=>array('index'),
	$model->is=>array('view','id'=>$model->is),
	'Update',
);

$this->menu=array(
	array('label'=>'Daftar Threadstar', 'url'=>array('index')),
	array('label'=>'Buat Threadstar', 'url'=>array('create')),
	array('label'=>'Liat Threadstar', 'url'=>array('view', 'id'=>$model->is)),
	array('label'=>'Pengaturan Thread', 'url'=>array('admin')),
);
?>

<h1>Update Threadstar <?php echo $model->is; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>