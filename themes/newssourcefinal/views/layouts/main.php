<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title><?php echo $this->pageTitle ?></title>

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/css/styles.css" type="text/css" media="screen" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl ?>/css/print.css" media="print" />

	<style type="text/css" media="screen">
		h1.fontface {font: 32px/38px 'MichromaRegular', Arial, sans-serif;letter-spacing: 0;}
		p.style1 {font: 18px/27px 'MichromaRegular', Arial, sans-serif;}
		@font-face {
			font-family: 'MichromaRegular';
			src: url('<?php echo Yii::app()->theme->baseUrl ?>/css/font/Michroma-webfont.eot');
			src: url('<?php echo Yii::app()->theme->baseUrl ?>/css/font/Michroma-webfont.eot?#iefix') format('embedded-opentype'),
			url('<?php echo Yii::app()->theme->baseUrl ?>/css/font/Michroma-webfont.woff') format('woff'),
			url('<?php echo Yii::app()->theme->baseUrl ?>/css/font/Michroma-webfont.ttf') format('truetype'),
			url('<?php echo Yii::app()->theme->baseUrl ?>/css/font/Michroma-webfont.svg#MichromaRegular') format('svg');
			font-weight: normal;
			font-style: normal;
		}
	</style>


	<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	
</head>
<body>


	  <header>
 <div class="container1">
    <!--start title-->
    <h1 class="fontface" id="title"><?php echo Yii::app()->name ?></h1>
	<!--end title-->
  </div>
    
	</header>
	<div class="container" id="page">
	<div id="mainmenu">
		<?php
		$kategoriAll=array(); 
		$kategori=Kategori::model()->findAll();
		foreach ($kategori as $i=>$ii)
		{
			$kategoriAll[]=array('label'=>$ii["kategori"],'url'=>array('/kategori/view','id'=>$ii["id"]));
		}
		$this->widget('ext.mbmenu.MbMenu',array(
			'items'=>array(
				array('label'=>'Home', 'url'=>array('/site/index')),
				array('label'=>'Forum', 'url'=>array('/kategori/index'),
					'items'=>$kategoriAll,
				),
				array('label'=>'Kelola Kategori', 'url'=>array('/kategori/admin'), 'visible'=>Yii::app()->user->getLevel()<=2),
				array('label'=>'Kelola Thread', 'url'=>array('/thread/admin'), 'visible'=>Yii::app()->user->getLevel()<=2),
				array('label'=>'Kelola Komentar', 'url'=>array('/comment/admin'), 'visible'=>Yii::app()->user->getLevel()<=2),
				array('label'=>'Kelola Berita', 'url'=>array('/news/admin'), 'visible'=>Yii::app()->user->getLevel()<=2),
				array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
				array('label'=>'Register', 'url'=>array('/user/create'), 'visible'=>Yii::app()->user->isGuest),
				array('label'=>'Profile ('.Yii::app()->user->name.')', 'url'=>array('/user/view','id'=>Yii::app()->user->id), 'visible'=>!Yii::app()->user->isGuest),
				array('label'=>'Logout', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
					),
				)); ?>
		</div>
    </nav>
	
			

		</aside><!-- end of sidebar1 -->
        

			<section id="content"><!-- #content -->

				<article class="group1">
					<?php echo $content ?>
				</article>

			<article class="group3">
							<h2><a href="#">First Interesting Section Title</a></h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. <a href="#">Duis sagittis ipsum</a>. Sed cursus ante dapibus diam. </p> 
						
						</article>
						
        		<article class="group3">
							<h2><a href="#">Second Section or Article Title</a></h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet.  </p>
						</article>
						<article class="group3">
							<h2><a href="#">First Interesting Section Title</a></h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. <a href="#">Duis sagittis ipsum</a>. Sed cursus ante dapibus diam. Sed nisi.</p> 

						</article>
					
					<?php /*
        		<article class="group2">						<br><br>
							<h2><a href="#">Second Section or Article Title</a></h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Fusce nec tellus sed augue semper porta. Mauris massa. Vestibulum lacinia arcu eget nulla. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Curabitur sodales ligula in libero. Sed dignissim lacinia nunc. Usu pericula argumentum repudiandae ea, ut vis deserunt oportere, assum nominavi percipitur ei ius. Nam timeam atomorum philosophia te, ut eum amet laoreet pertinax, an duo error aperiri reformidans. Et alii quas partiendo mei, wisi quaestio convenire et vix.</p>
						</article>
						
						<div class="newsbox">
<h2> Occupy Wallstreet Movement- Going Global</h2>
 <img src="<?php echo Yii::app()->theme->baseUrl ?>/images/occupy1.jpg" class="floatimgleft">
<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. 
Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit
 amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>

<a href="#" class="button"> Read More </a>
    </div>
	*/ ?>
</section>

	

	</section><!-- end of #main content and sidebar-->
->
</body>
</html>
